#!/bin/bash
RHOST=$1
echo $RHOST
DOWNLOAD_DIR=`ssh $RHOST "cat /etc/transmission-daemon/settings.json" | grep download-dir | awk -F ":" '{print $2}' | sed -r 's/\s*//g' | sed -r 's/\"//g' | sed -r 's/\,//g' `
echo $DOWNLOAD_DIR
transmission-remote $RHOST -l  |cut -c70- | awk '{print $1}' | sed -r '/^\s*$/d' | sed 1d | sort > /tmp/tr_${RHOST}.log
echo $?
ssh $RHOST "ls -trl ${DOWNLOAD_DIR}" | awk '{print $9}' | sed -r '/^\s*$/d' | sort > /tmp/f_${RHOST}.log
echo "Files only in host (not in transmission:)"
echo "----------------------------------------"
comm -23 /tmp/f_${RHOST}.log /tmp/tr_${RHOST}.log

