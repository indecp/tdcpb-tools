#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4
#
#
# Copyright Nicolas Bertrand (nicolas@indecp.org), 2016
#
# This file is part of tdcpb-tools.
#
#    tdcpb-tools is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tdcpb-tools is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tdcpb-tools.  If not, see <http://www.gnu.org/licenses/>.
#
#
# Usage:
# cf tdcpb-remove-torrent -h
#
# Remove a torrent from a torrent client, delete of file on host can be forced

import sys
import re
sys.path.append('.')
import argparse
import getpass
import os.path
import pprint

import paramiko

from tdcpblib import logger
from tdcpblib.torrent_meta import TorrentMeta
from tdcpblib.common import get_host_torrent_login
from tdcpblib.common import TdcpbException
from tdcpblib.common import sizeof_fmt
from tdcpblib.torrent_client import TransmissionClient

DCPS_SOURCE_CONFIG_FILE = './config/tdcpb-dcp-path'


def open_config_dcp_path(path):
    _dict = {}
    try:
        with open(path, 'r') as f:
            lines = f.readlines()
        DCP_SOURCE_PATH={}
        _tmp = [ _l.split(':') for _l in lines]
        for _l  in _tmp:
            _dict[_l[0].strip()] = _l[1].strip()
        return _dict

    except IOError as err:
        print err
        return None


def ssh_remove(p_host, download_dir, p_DCP): #, dcp_files):
    passphrase = None

    if download_dir is None:
        # Use the custom config file as fallback.
        DCPS_SOURCE_PATH = open_config_dcp_path(DCPS_SOURCE_CONFIG_FILE)
        if DCPS_SOURCE_PATH is None:
            _msg='read of DCP path config goes wrong'
            raise TdcpbException(_msg)

        if p_host in DCPS_SOURCE_PATH:
            download_dir = os.path.join(DCPS_SOURCE_PATH[p_host], p_DCP)
        else:
            _msg = "No DCP source found for {}".format(p_host)
            raise TdcpbException(_msg)

    download_dir = os.path.join(download_dir, p_DCP)
    cmd_size = 'du -sh {}'.format(download_dir)
    cmd_print = 'find {} -print'.format(download_dir)
    cmd_delete = 'find {} -delete'.format(download_dir)

    client = paramiko.SSHClient()
    client._policy = paramiko.WarningPolicy()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    ssh_config = paramiko.SSHConfig()
    user_config_file = os.path.expanduser("~/.ssh/config")
    if os.path.exists(user_config_file):
        with open(user_config_file) as f:
            ssh_config.parse(f)

    host_config = ssh_config.lookup(p_host)
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(host_config['hostname'], username=host_config['user'], port= int(host_config['port']))
    except paramiko.ssh_exception.PasswordRequiredException as error:
        print('WARNING: {}'.format(error.message))
        # 3 password attempts allowed.
        tries = 3
        while tries > 0:
            passphrase = getpass.getpass('Please enter your SSH key passphrase:')
            try:
                ssh.connect(host_config['hostname'],
                            username=host_config['user'],
                            port= int(host_config['port']),
                            passphrase=passphrase)
                break
            except paramiko.ssh_exception.SSHException as error:
                logger.error('SSH connection failed: {}'.format(error.message))
                tries -= 1
        else:
            logger.error('SSH connection failed. Data was not deleted.')
            raise

    # Compute size of removed data.
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmd_size)
    size = re.split('\s+', ssh_stdout.read())[0]

    # Check access of removed data.
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmd_print)
    ssh_stdin.close()
    lines = ssh_stderr.read().splitlines()
    if len(lines) > 0:
        for line in lines:
            _msg = '%s: %s' % (p_host, line)
            logger.error(_msg)
        _msg = "Errors during ssh cmd print"
        logger.error(_msg)
        raise TdcpbException(_msg)

    lines = ssh_stdout.read().splitlines()
    if len(lines) > 0:
        _msg = "STDOUT"
        logger.info(_msg)
        for line in lines:
            _msg = '%s: %s' % (p_host, line)
            logger.info(_msg)
        _msg = "END STDOUT"
        logger.info(_msg)

    # Actually delete the data.
    response = 'y'
    if response == 'y':
        if passphrase:
            ssh.connect(host_config['hostname'],
                        username=host_config['user'],
                        port= int(host_config['port']),
                        passphrase=passphrase)
        else:
            ssh.connect(host_config['hostname'], username=host_config['user'], port= int(host_config['port']))
        logger.info("Delete cmd: {}".format(cmd_delete))
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmd_delete)
        ssh_stdin.close()
        lines = ssh_stdout.read().splitlines()
        if len(lines) > 0:
            for line in lines:
                _msg = '%s: %s' % (p_host, line)
                logger.error(_msg)
            _msg = "Errors during ssh cmd delete"
            logger.error(_msg)
            raise TdcpbException(_msg)
        lines = ssh_stdout.read().splitlines()
        if len(lines) > 0:
            _msg = "STDOUT"
            logger.info(_msg)
            for line in lines:
                _msg = '%s: %s' % (p_host, line)
                logger.info(_msg)
            _msg = "END STDOUT"
            logger.info(_msg)
    else:
        _msg =  "Nothing to delete"
        logger.info("msg")
        return '0'
    return size


def main(argv):
    description='Remove torrent in host with torrent file or name (with -n option)'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('host',
                metavar='HOSTNAME',
                type = str,
                help = 'hostname where torrent will be removed' )
    parser.add_argument('torrent_file',
                metavar='TORRENT_FILE',
                type = str,
                help = 'Path to torrent file or torrent name' )

    parser.add_argument('-f', '--force',
                    action='store_true',
                   help='remove torrent data on bittorrent client host')
    parser.add_argument('-n', '--name',
                    action='store_true', dest='by_name',
                   help='Select torrent by name instead of file path')

    args = parser.parse_args()
    remove_torrent = True

    if args.force:
        logger.info('Remove content option activated')

    tinfo = None
    if not args.by_name:
        if not os.path.exists(args.torrent_file):
            logger.error('Invalid torrent path: {}'.format(args.torrent_file))
            return 1
        try:
            tinfo = TorrentMeta.info(args.torrent_file)
        except TdcpbException as err:
            logger.error('{}'.format(err))
            return 1

    try :
        user, account, password = get_host_torrent_login(args.host)
    except TdcpbException as err:
        logger.error('{}'.format(err))
        return 1

    _tc = TransmissionClient()
    try:
        _tc.connect(
                address = args.host,
                port = 9091,
                user = user,
                password = password)
    except TdcpbException as err:
        logger.error('{}'.format(err))
        return 1
    _torrents = _tc.get_torrents()['torrents']
    torrent_name  = None
    torrent_hash  = None
    torrent_files = None
    torrent_size  = None
    download_dir  = None
    if args.by_name:
        for _t in _torrents:
            if _t['name'] == args.torrent_file:
                torrent_name  = args.torrent_file
                torrent_hash  = _t['hash']
                torrent_files = _t['files']
                torrent_size  = _t['size']
                download_dir  = _t['download-dir']
                break
        if torrent_hash is None:
            err = "Torrent {} not found".format(args.torrent_file)
            if args.force:
                # Torrent does not exist but maybe data exist.
                remove_torrent = False
                logger.info(err)
                download_dir = _tc.get_download_dir()
            else:
                logger.error('{}'.format(err))
                return 1
    else:
        if not any( ((_t['hash'] == tinfo['hash']) and   (_t['name'] == tinfo['name'])) for _t in _torrents):
            err = "Torrent {} not found".format(tinfo['name'])
            if args.force:
                # Torrent does not exist but maybe data exist.
                remove_torrent = False
                logger.info(err)
                download_dir = _tc.get_download_dir()
            else:
                logger.error('{}'.format(err))
                return 1
        else :
            torrent_name  = tinfo['name']
            torrent_hash  = tinfo['hash']
            torrent_files = tinfo['files']
            torrent_size  = tinfo['size']
            download_dir  = tinfo['download-dir']

    if remove_torrent:
        print('The torrent {} (hash: {}) will be removed'.format(torrent_name, torrent_hash))
        _tc.remove(torrent_hash, delete_data=False)

    _torrents = _tc.get_torrents()['torrents']
    if not any( _t['hash'] == torrent_hash for _t in _torrents):
        if args.force:
            if args.by_name:
                # We assume the torrent name was the directory name, which is most probably not always true.
                DCP = args.torrent_file
            else:
                DCP = os.path.basename(os.path.splitext(args.torrent_file)[0])

            try:
                size = ssh_remove(args.host, download_dir, DCP)
            except (TdcpbException, paramiko.ssh_exception.SSHException) as error:
                logger.error('Errors detected during ssh_remove')
                return 1
            else:
                if torrent_size is not None:
                    msg= u'Torrent {} removed and {} freed in {}'.\
                            format(torrent_name, sizeof_fmt(torrent_size), args.host)
                else:
                    msg= u'Torrent {} removed and {} freed in {}'.\
                            format(torrent_name, size, args.host) # TODO
                logger.info(msg)
    else:
        msg= u'Removal of torrent {} in {} failed'.format(torrent_name, args.host)
        logger.error(msg)
        return 1

    msg= u'Torrent {} removed in {}'.format(torrent_name, args.host)
    logger.info(msg)
    return 0

if __name__ == "__main__":
   sys.exit(main(sys.argv))

