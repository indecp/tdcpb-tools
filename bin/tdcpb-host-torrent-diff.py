#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4

import logging
import sys
import json
import argparse
import netrc

from transmissionrpc import Client, HTTPHandlerError, TransmissionError
from operator import itemgetter

TRANSMISSION_CLIENT = "transmission"
DELUGE_CLIENT       = "deluge"

CLIENT_TYPE = [
        TRANSMISSION_CLIENT,
        DELUGE_CLIENT]

TDCPB31 = {
    'host' : '10.10.10.31',
    'login': 'tdcpb',
    'password': 'tdcpbl0v3'
    }
GNCR = {
    'host' : '10.10.10.175',
    'login': 'tdcpb',
    'password': 'tdcpbl0v3'
    }
#GNCR = {
#   'host' : 'localhost',
#    'login': 'tdcpb',
#    'password': 'tdcpbl0v3'
#    }



class BitTorrentClient(object):
    def __init__(self,client_type = TRANSMISSION_CLIENT):
        self.client_type= client_type
    def connect(self, address, port, user, password):
        pass
    def get_torrents(self):
        pass
    def add_torrent(self, torrent_path):
        pass
    def del_torrent(self, torrent_name):
        pass

class TransmissionClient(BitTorrentClient):
    def __init__(self):
        BitTorrentClient.__init__(self, TRANSMISSION_CLIENT)
        self.dict_client={}

    def connect(self, address, port, user, password):
        try :
           self.client = Client( address, port, user, password)
        except TransmissionError as err:
            logging.error(u'TransmissionError {} on {} {}'.format(err, address, port))
            raise

        self.dict_client['name']= address

    def get_torrents(self):
        tr_torrents = self.client.get_torrents()
        self.dict_client['torrents']= []
        for _torrent in tr_torrents:
            _torrent = {
            u'name'    : _torrent.name,
            u'hash'    : _torrent.hashString,
            u'progress': _torrent.progress,
            u'status'  : _torrent.status,
            u'addedDate'  : _torrent.date_added
            }
            self.dict_client['torrents'].append(_torrent)
        return self.dict_client
    def add_torrent(self, torrent_path):
        pass
    def del_torrent(self, torrent_name):
        pass




def compare_hosts(host1,host2):

    _netrc = netrc.netrc()
    login, account, password = _netrc.authenticators(host1)

    tc_host1 = TransmissionClient()
    tc_host1.connect(
        host1,
        port=9091,
        user = login,
        password = password)
    _torrents_host1 = tc_host1.get_torrents()[u'torrents']

    login, account, password = _netrc.authenticators(host2)

    tc_host2 = TransmissionClient()
    tc_host2.connect(
        host2,
        port=9091,
        user = login,
        password = password)
    _torrents_host2 = tc_host2.get_torrents()[u'torrents']

    _common = []
    for _t in _torrents_host1:
        res = (item for item in _torrents_host2 if item["hash"] == _t['hash'])
        for i in res:
            _common.append(i)
    newlist = sorted(_common, key=itemgetter(u'status', u'addedDate'))
    print "Torrents in {} and {}".format(host1, host2)
    for item in newlist:
        print "  {} {:<14}({}) {} ".format(item[u'addedDate'], item['status'],host2, item['name'])
    print "\n"

    _not_in_host1 = []
    for _t2 in _torrents_host2:
        _match = False
        for _t1 in _torrents_host1:
            if _t1['hash'] == _t2['hash']:
                _match=True
        if not _match:
            _not_in_host1.append(_t2)
    newlist = sorted(_not_in_host1, key=itemgetter(u'status', u'addedDate'))
    print "Torrents not  in {}".format(host1)
    for item in newlist:
        print "  {} {:<14}({}) {} ".format(item[u'addedDate'], item['status'],host2, item['name'])
    print "\n"

    _not_in_host2 = []
    for _t1 in _torrents_host1:
        _match = False
        for _t2 in _torrents_host2:
            if _t1['hash'] == _t2['hash']:
                _match=True
        if not _match:
            _not_in_host2.append(_t1)
    newlist = sorted(_not_in_host2, key=itemgetter(u'status', u'addedDate'))
    print "Torrents not  in {}".format(host2)
    for item in newlist:
        print "  {} {:<14}({}) {} ".format(item[u'addedDate'], item['status'],host1, item['name'])



def temp():
    _common = []
    for _t in _torrents_gncr['torrents']:
        res = (item for item in _torrents_tdcpb31['torrents'] if item["hash"] == _t['hash'])
        for i in res:
            _common.append(i)
    newlist = sorted(_common, key=itemgetter(u'addedDate')) 
    #print(json.dumps(newlist, indent=2))
    for item in newlist:
        print "{} {} {} ".format(item[u'addedDate'], item['status'], item['name'])

    print "Torent NOT in tdcpb31"
    not_in_tdcpb31 = []
    for _tgncr in _torrents_gncr['torrents']:
        _match = False
        for _ttdcpb31 in _torrents_tdcpb31['torrents']:
            if _tgncr['hash'] == _ttdcpb31['hash']:
                _match=True
        if _match == False:
            print "{} {}".format(_tgncr[u'addedDate'], _tgncr['name'])


def main(argv):
    host1 = None
    host2 = None

    parser = argparse.ArgumentParser(description='Compare torrents in 2 hosts')
    parser.add_argument('host1',
        metavar='HOST1',
        type = str,
        help = 'FIRST HOST')
    parser.add_argument('host2',
        metavar='HOST2',
        type = str,
        help = 'SECOND  HOST ' )
    parser.add_argument('-d', '--debug', dest='debug', action='store_const',
        const=logging.DEBUG, default=logging.INFO,
        help='debug mode')

    args = parser.parse_args()


    logging.basicConfig(format = '%(asctime)s - %(levelname)s - %(message)s - %(filename)s %(funcName)s line %(lineno)d thread %(thread)d/%(threadName)s',
                         level= logging.ERROR)

    _netrc = netrc.netrc()
    if args.host1 in _netrc.hosts:
        host1 = args.host1
    else:
        logging.error(u'Unknow host {}'.format(args.host1))
        return 1
    if args.host2 in _netrc.hosts:
        host2 = args.host2
    else:
        logging.error(u'Unknow host {}'.format(args.host2))
        return 1

    compare_hosts(host1, host2)


    return 0
if __name__ == "__main__":
   sys.exit(main(sys.argv))


